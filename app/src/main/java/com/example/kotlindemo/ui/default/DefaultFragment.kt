package com.example.kotlindemo.ui.default

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.kotlindemo.R

class DefaultFragment : Fragment() {

    private lateinit var defaultViewModel: DefaultViewModel
    private var firstOpen: Number ?= 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        defaultViewModel =
            ViewModelProviders.of(this).get(DefaultViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.text_default)
        defaultViewModel.text.observe(this, Observer {
            textView.text = it
        })

        // Toast.makeText(context, "hahó, default fragment onCreateView", Toast.LENGTH_LONG).show()

        /*if(firstOpen == 0) {
            // entitynév lekérés
            val db = Database()
            db.addDemoData()
            Toast.makeText(context, "elkezdtem hozzáadni a adatot", Toast.LENGTH_LONG).show()

            firstOpen = 1
        }*/

        return root
    }



}