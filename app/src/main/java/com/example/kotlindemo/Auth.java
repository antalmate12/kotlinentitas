package com.example.kotlindemo;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class Auth {
    private FirebaseAuth mAuth;

    public Auth() {
        mAuth = FirebaseAuth.getInstance();
    }

    // Choose authentication providers
    public List<AuthUI.IdpConfig> authUIProviders = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.PhoneBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build()
            /*AuthUI.IdpConfig.FacebookBuilder().build(),
            AuthUI.IdpConfig.TwitterBuilder().build()*/
    );



    public FirebaseAuth getmAuth() {
        return mAuth;
    }

    public void signOut() {
        mAuth.signOut();
    }
}
