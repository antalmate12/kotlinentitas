package com.example.kotlindemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    private val RC_SIGN_IN = 123 //the request code could be any Integer
    private val a = Auth()
    override fun onStart() {
        super.onStart()
        val mAuth: FirebaseAuth = FirebaseAuth.getInstance()

        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
        if (currentUser !== null) {
            Toast.makeText(
                applicationContext,
                "Már bent vagy " + FirebaseAuth.getInstance().currentUser?.displayName.toString(),
                Toast.LENGTH_LONG
            ).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)


        myButton1.setOnClickListener {

            // Create and launch sign-in intent
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    // Teszteléshez van bent, Ha kész vedd ki
                    // ------------------------------
                    .setIsSmartLockEnabled(false)
                    // ------------------------------
                    .setTheme(R.style.Theme_AppCompat)
                    .setAvailableProviders(a.authUIProviders)
                    .build(),
                RC_SIGN_IN
            )
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            // val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser
                if (user != null) {
                    Toast.makeText(
                        applicationContext,
                        "Be vagy jelentkezve" + user.displayName,
                        Toast.LENGTH_LONG
                    ).show()
                }


                val i = Intent(this, MainActivity::class.java)
                startActivity(i)

                // ...
            } else {
                Toast.makeText(applicationContext, "Hát ezt beszoptad", Toast.LENGTH_LONG).show()

                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }


}
